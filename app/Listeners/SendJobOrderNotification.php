<?php

namespace App\Listeners;

use App\Events\JobOrderCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendJobOrderNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\JobOrderCreated  $event
     * @return void
     */
    public function handle(JobOrderCreated $event)
    {
        //
    }
}
