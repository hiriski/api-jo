<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\Image as ModelImage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;

class UploadImage
{

  /**
   * Upload multiple image sizes.
   * @param string  $image - base64 image
   * @param string directory
   * @return
   */
  public static function handle($image, $directory = '', $multiResolution = true)
  {
    $images = [];

    $destinationPath    = $directory
      ? storage_path(ModelImage::DESTINATION_PATH . $directory . '/')
      : storage_path(ModelImage::DESTINATION_PATH . '/');

    if (!File::isDirectory($destinationPath)) {
      File::makeDirectory($destinationPath, 0777, true, true);
    }

    try {
      $fileName = Str::random(10) . time(); // it's should define in first for loop.
      $img      = Image::make($image);

      // Upload multi resolution.
      if ($multiResolution) {
        foreach (ModelImage::IMAGE_SIZES as $variantSize => $width) {
          if ($variantSize !== 'original')
            $img->resize($width, null, function ($constraint) {
              $constraint->aspectRatio();
            });

          $img->save(
            $destinationPath . $fileName . '_' . $variantSize . '.' . ModelImage::IMAGE_EXTENSION,
            100,
          );

          $images = [
            'image_original'  => '/' . $directory . '/' . $fileName . '_' . 'original' . '.' . ModelImage::IMAGE_EXTENSION,
            'image_xs'        => '/' . $directory . '/' . $fileName . '_' . 'xs'       . '.' . ModelImage::IMAGE_EXTENSION,
            'image_sm'        => '/' . $directory . '/' . $fileName . '_' . 'sm'       . '.' . ModelImage::IMAGE_EXTENSION,
            'image_md'        => '/' . $directory . '/' . $fileName . '_' . 'md'       . '.' . ModelImage::IMAGE_EXTENSION,
            'image_lg'        => '/' . $directory . '/' . $fileName . '_' . 'lg'       . '.' . ModelImage::IMAGE_EXTENSION,
          ];
        }

        return $images;

        // Upload original image.
      } else {
        $img->save(
          $destinationPath . $fileName . '.' . ModelImage::IMAGE_EXTENSION,
          90,
        );
        return $directory
          ? '/' . $directory . '/' . $fileName . '.' . ModelImage::IMAGE_EXTENSION
          : $fileName . '.' . ModelImage::IMAGE_EXTENSION;
      }
    } catch (\Exception $exception) {
      return response()->json([
        'message'   => $exception->getMessage()
      ]);
    }
  }
}
