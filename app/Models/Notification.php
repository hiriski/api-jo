<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = [
        'body',
        'notification_type_id'
    ];

    public function type()
    {
        return $this->belongsTo(NotificationType::class);
    }
}
