<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory, Uuids;

    /**
     * The attribute that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'label',
        'address_details',
        'district',
        'city',
        'province',
        'postcode',
        'country',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'country'   => "indonesia", // default country
    ];

    public function customer()
    {
        return $this->belongsToMany(Address::class, 'customer_address');
    }
}
