<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    use HasFactory;

    public $timetamps = false;

    public const UNPAID = 1;
    public const DOWN_PAYMENT = 2;
    public const PAID = 3;

    /**
     * The attribute that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
    ];

    public function jobOrders()
    {
        return $this->hasMany(JobOrder::class);
    }
}
