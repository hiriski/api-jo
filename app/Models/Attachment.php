<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    use HasFactory, Uuids;

    /**
     * The attribute that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'file_type',
        'url',
    ];
}
