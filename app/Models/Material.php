<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends Model
{
    use HasFactory, SoftDeletes;

    const IN_STOCK = 'In stock';
    const OUT_OF_STOCK = 'Out of stock';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'unit',
        'display_unit',
        'image_url',
        'material_brand_id',
        'material_category_id',
        'stock_length',
        'stock_width',
        'stock_pcs',
        'width_per_roll',
        'length_per_roll',
        'price',
        'description',
        'status'
    ];

    // protected $with = ['brand'];

    protected $appends = ['status_stock'];

    public function getStatusStockAttribute()
    {
        return $this->stock_length >= 5000 ? self::IN_STOCK : self::OUT_OF_STOCK;
    }

    /**
     * Scope a query to only include material with status active.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsActive($query)
    {
        return $query->where('status', 'active');
    }

    /**
     * Scope a query to only include material with status inactive.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsInActive($query)
    {
        return $query->where('status', 'inactive');
    }

    public function jobOrders()
    {
        return $this->belongsToMany(JobOrder::class);
    }

    public function brand()
    {
        return $this->belongsTo(MaterialBrand::class, 'material_brand_id');
    }

    public function category()
    {
        return $this->belongsTo(MaterialCategory::class, 'material_category_id');
    }
}
