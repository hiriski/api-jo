<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobOrderStatus extends Model
{
    use HasFactory;

    public $timetamps = false;

    public const ACTIVE     = 1;
    public const INACTIVE   = 2;
    public const REJECTED   = 3;

    /**
     * The attribute that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function jobOrders()
    {
        return $this->hasMany(JobOrder::class);
    }
}
