<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory, Uuids;

    public const IMAGE_QUALITY = 90;
    public const IMAGE_EXTENSION = 'jpg';
    public const DESTINATION_PATH = 'app/public/images/';

    public const IMAGE_SIZES = [
        'original'  => null,
        'lg'        => 1000,
        'md'        => 600,
        'sm'        => 340,
        'xs'        => 120,
    ];

    protected $fillable = [
        'image_xs',
        'image_sm',
        'image_md',
        'image_lg',
        'image_original',
        'user_id',
        'imageable_id',
        'imageable_type',
    ];

    /**
     * Get the parent commentable model (post or video).
     */
    public function imageable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
