<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory, SoftDeletes, Uuids;

    /**
     * The attribute that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'phone_number',
        'email',
        'photo_url',
        'address_id',
        'notes'
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'customer_type'   => "individual", // default country
    ];

    /**
     * eager load relationship for every query
     * @var array
     */
    protected $with = [
        'addresses',
    ];

    public function jobOrder()
    {
        return $this->hasMany(JobOrder::class);
    }

    public function addresses()
    {
        return $this->belongsToMany(Address::class, 'customer_address');
    }
}
