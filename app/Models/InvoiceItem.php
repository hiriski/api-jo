<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    use HasFactory, Uuids;

    protected $fillable = [
        'description',
        'notes',
        'invoice_id',
        'quantity',
        'price',
        'discount'
    ];

    protected $appends = [
        'total'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'quantity'  => 'integer',
        'price'     => 'integer',
        'discount'  => 'integer',
    ];

    public function getTotalAttribute()
    {
        if ($this->quantity && $this->price) {
            return $this->quantity * $this->price;
        } else {
            return null;
        }
    }


    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
