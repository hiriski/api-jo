<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaterialBrand extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * The attribute that are mass assignable.
     * @var array
     */
    protected $fillable = ['name', 'description', 'image_url'];

    protected $appends = ['material_counts'];

    public function materials()
    {
        return $this->hasMany(Material::class, 'material_brand_id');
    }

    public function getMaterialCountsAttribute()
    {
        return $this->materials->count();
    }
}
