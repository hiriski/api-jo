<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class JobOrder extends Model
{
    use HasFactory, SoftDeletes, Uuids;

    public const PREFIX_ORDER_CODE = '#JO';

    /**
     * The attribute that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'created_by_user_id',
        'updated_by_user_id',
        'product_id',
        'customer_id',
        'status_id',
        'category_id',
        'payment_status_id',
        'payment_method_id',
        'bank_account_id',
        'production_status_id',
        'is_ready_to_production',
        'title',
        'order_number',
        'order_date',
        'due_date',
        'body',
        'down_payment',
        'dimension_material_length',
        'dimension_material_width',
        'quantity',
        'price',
        'tax',
        'additional_price_name',
        'additional_price',
        'rejected_by_user_id',
        'reason_rejected',
        'is_approved',
        'percentage_progress',
        'progress_by_user_id'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'status_id'                 => 'integer',
        'production_status_id'      => 'integer',
        'category_id'               => 'integer',
        'payment_status_id'         => 'integer',
        'payment_method_id'         => 'integer',
        'bank_account_id'           => 'integer',
        'down_payment'              => 'integer',
        'order_number'              => 'string',
        'dimension_material_length' => 'integer',
        'dimension_material_width'  => 'integer',
        'quantity'                  => 'integer',
        'price'                     => 'integer',
        'tax'                       => 'integer',
        'additional_price'          => 'integer',
        'percentage_progress'       => 'integer',
        'is_approved'               => 'boolean',
        'is_ready_to_production'    => 'boolean'
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'status_id'             => 1, // default is active
        'production_status_id'  => 1, // waiting
        'payment_status_id'     => 1, // unpaid
        'percentage_progress'   => 0, // default 0,
        'is_approved'           => 1, // default true
        'payment_method_id'     => 1, // default cash
        'is_ready_to_production' => true // default true
    ];

    protected $appends = [
        'total'
    ];

    /**
     * eager load relationship for every query
     * @var array
     */
    protected $with = [
        'status',
        'production_status',
        'category',
        'product',
        'customer',
        'created_by',
        'updated_by',
        'progress_by',
        'rejected_by',
        'payment_status',
        'payment_method',
        'bank_account',
        'materials',
        'machines',
        'attachments',
        'notes',
        'images',
        'comments',
    ];


    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {

            /** Override Uuids trait */
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid()->toString();
            }

            /** Jo number */
            $model->order_number = (string) self::PREFIX_ORDER_CODE . time();
        });
    }

    public function getTotalAttribute()
    {
        if ($this->quantity && $this->price) {
            return $this->quantity * $this->price;
        } else {
            return null;
        }
    }

    /**
     * Scope a query to only include order ready to production.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeReadyToProduction($query)
    {
        // return $query->where('payment_status_id', '=', PaymentStatus::PAID)
        //     ->orWhere('payment_status_id', '=', PaymentStatus::DOWN_PAYMENT);
        return $query->where('is_ready_to_production', 1);
    }

    /**
     * Scope a query to only include job order with production status done.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProductionStatusDone($query)
    {
        return $query->where('production_status_id', '=', JobOrderProductionStatus::DONE);
    }

    /**
     * Scope a query to only include job order status active.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status_id', JobOrderStatus::ACTIVE);
    }

    /**
     * Scope a query to only include job order with status inactive.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInactive($query)
    {
        return $query->where('status_id', JobOrderStatus::INACTIVE);
    }

    /**
     * Scope a query to only include job order with status rejected.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRejected($query)
    {
        return $query->where('status_id', JobOrderStatus::REJECTED);
    }

    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_user_id');
    }

    public function updated_by()
    {
        return $this->belongsTo(User::class, 'updated_by_user_id');
    }

    public function progress_by()
    {
        return $this->belongsTo(User::class, 'progress_by_user_id');
    }

    public function rejected_by()
    {
        return $this->belongsTo(User::class, 'rejected_by_user_id');
    }

    public function status()
    {
        return $this->belongsTo(JobOrderStatus::class, 'status_id');
    }

    public function production_status()
    {
        return $this->belongsTo(JobOrderProductionStatus::class, 'production_status_id');
    }

    public function category()
    {
        return $this->belongsTo(JobOrderCategory::class, 'category_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'category_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function payment_status()
    {
        return $this->belongsTo(PaymentStatus::class);
    }

    public function payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function bank_account()
    {
        return $this->belongsTo(BankAccount::class);
    }

    public function materials()
    {
        return $this->belongsToMany(Material::class, 'material_job_order', 'job_order_id', 'material_id');
    }

    public function machines()
    {
        return $this->belongsToMany(Machine::class, 'job_order_machine', 'job_order_id', 'machine_id');
    }

    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'attachment_job_order', 'job_order_id', 'attachment_id');
    }

    public function notes()
    {
        return $this->morphMany(Notes::class, 'noteable');
    }

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class, 'invoice_jo', 'jo_id', 'invoice_id');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
