<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaterialPaper extends Model
{
    use HasFactory;

    const IN_STOCK = 'In stock';
    const OUT_OF_STOCK = 'Out of stock';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'brand_id',
        'stock_length',
        'material_width',
        'length_per_roll',
        'price',
        'description'
    ];

    protected $with = ['brand'];

    protected $appends = ['status'];

    public function getStatusAttribute()
    {
        return $this->stock_length >= 5000 ? self::IN_STOCK : self::OUT_OF_STOCK;
    }

    public function jobOrders()
    {
        return $this->hasMany(JobOrder::class);
    }

    public function brand()
    {
        return $this->belongsTo(BrandMaterialPaper::class, 'brand_id');
    }
}
