<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory, Uuids;

    public const PREFIX_INVOICE_NUMBER = "INV-";

    protected $fillable = [
        'invoice_number',
        'bill_to_name',
        'bill_to_address',
        'bill_to_phone_number',
        'invoice_date',
        'invoice_due_date',
        'tax',
        'bill_discount',
        'notes',
        'terms_and_conditions'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'tax'           => 'integer',
        'bill_discount' => 'integer',
    ];

    protected $with = ['items'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $latestRecordId = $model->orderBy('id', 'desc')->latest()->first();
            if ($latestRecordId) {
                $model->invoice_number = (string) self::PREFIX_INVOICE_NUMBER . ($latestRecordId->id + 1);
            } else {
                $model->invoice_number = (string) self::PREFIX_INVOICE_NUMBER . '1';
            }
        });
    }

    public function items()
    {
        return $this->hasMany(InvoiceItem::class);
    }

    public function jobOrders()
    {
        return $this->belongsToMany(JobOrder::class, 'invoice_jo', 'jo_id', 'invoice_id');
    }
}
