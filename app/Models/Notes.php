<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    use HasFactory, Uuids;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notes';

    protected $fillable = ['body', 'user_id', 'noteable_id', 'noteable_type'];

    protected $with = ['user'];

    /**
     * Get the parent commentable model (post or video).
     */
    public function noteable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
