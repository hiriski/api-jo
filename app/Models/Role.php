<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public $timestamps = false;

    public const ROLE_ID_SUPER_ADMIN        = 1;
    public const ROLE_ID_ADMIN              = 2;
    public const ROLE_ID_WAREHOUSE_TEAM     = 3;
    public const ROLE_ID_PRODUCTION_TEAM    = 4;
    public const ROLE_ID_OWNER              = 5;
    public const ROLE_ID_MANAGEMENT         = 6;
    public const ROLE_ID_CASHIER            = 7;
    public const ROLE_ID_CUSTOMER_SERVICE   = 8;
    public const ROLE_ID_MAINTAINER         = 9;

    /**
     * The attribute that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
    ];

    protected $with = ['permissions'];

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permission');
    }
}
