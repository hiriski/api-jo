<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobOrderProductionStatus extends Model
{
    use HasFactory;

    public $timetamps = false;

    public const WAITING        = 1;
    public const IN_PROGRESS    = 2;
    public const DONE           = 3;
    public const HOLD           = 4;
    public const REJECTED       = 5;

    /**
     * The attribute that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
    ];

    public function jobOrders()
    {
        return $this->hasMany(JobOrder::class);
    }
}
