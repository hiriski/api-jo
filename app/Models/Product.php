<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory, SoftDeletes, Uuids;

    private const STATUS_ACTIVE = 'active';
    private const STATUS_DRAFT = 'draft';
    private const STATUS_INACTIVE = 'inactive';

    protected $fillable = ['title', 'slug', 'user_id', 'category_id', 'description', 'body', 'price', 'status'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {

            /** Override Uuids trait */
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }

            if (!$model->slug) {
                /** Create slug */
                $model->slug = (string) Str::slug($model->title) . '-' . time();
            }
        });
    }

    protected $with = ['images', 'category', 'tags'];

    protected $attributes = [
        'status'    => Product::STATUS_ACTIVE
    ];

    /**
     * Scope a query to only include Product with status active.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * Scope a query to only include Product with status draft.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDraft($query)
    {
        return $query->where('status', self::STATUS_DRAFT);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function tags()
    {
        return $this->morphMany(Tag::class, 'taggable');
    }
}
