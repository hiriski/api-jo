<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'status', 'image'];

    protected $attributes = [
        'status' => 'active'
    ];

    public function jobOrders()
    {
        return $this->hadMany(JobOrder::class);
    }
}
