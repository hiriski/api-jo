<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobOrderCategory extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name', 'slug', 'icon', 'description'];

    public function jobOrders()
    {
        return $this->hasMany(JobOrder::class);
    }
}
