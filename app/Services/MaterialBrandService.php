<?php

namespace App\Services;

use App\Http\Requests\Warehouse\StoreMaterialBrandRequest;
use App\Models\MaterialBrand;
use App\UploadImage;
use Illuminate\Http\Request;

class MaterialBrandService
{

  /**
   * Find all materials.
   */
  public function findAll()
  {
    $materialBrands = MaterialBrand::orderBy('name', 'desc')->with(['materials'])->get();
    return  $materialBrands;
  }

  /**
   * Search product by keywords.
   */
  public function searchByKeyword($searchTerm)
  {
    $materialBrands = MaterialBrand::orderBy('name', 'desc')->with(['materials']);

    // Query search.
    return $materialBrands->where(function ($q) use ($searchTerm) {
      $q->where('name', 'LIKE', '%' . $searchTerm . '%');
      $q->orWhere('description', 'LIKE', '%' . $searchTerm . '%');
    })->get();
  }

  /**
   * Find by id
   */
  public function findById(string $id)
  {
    return MaterialBrand::with(['materials'])->findOrFail($id);
  }


  /**
   * Create product.
   */
  public function create(StoreMaterialBrandRequest $request)
  {
    $data = $request->only([
      'name',
      'unit_material',
      'description',
      'image'
    ]);

    $image_url = null;

    if ($request->has('image')) {
      $image_url  = UploadImage::handle($data['image'], 'materials/brands', false);
    }

    $newMaterialBrand = MaterialBrand::create(array_merge(
      $data,
      ['image_url' => $image_url]
    ));

    // return fresh new material
    return MaterialBrand::findOrFail($newMaterialBrand->id);
  }

  /**
   * Update product..
   */
  public function update(Request $request, string $id)
  {
    $data = $request->only([
      'name',
      'material_unit',
      'description',
      'new_image'
    ]);

    // Find product by id.
    $materialBrand = MaterialBrand::findOrFail($id);

    // Image.
    $image_url = $materialBrand->image_url;

    if ($request->has('new_image')) {
      $image_url  = UploadImage::handle($data['new_image'], 'materials/brands', false);
    }

    $materialBrand->update(array_merge(
      $data,
      ['image_url' => $image_url]
    ));

    return $materialBrand->fresh();
  }

  /**
   * Create product.
   */
  public function delete(string $id)
  {
    return MaterialBrand::destroy($id);
  }
}
