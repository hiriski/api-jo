<?php

namespace App\Services;

use Illuminate\Http\JsonResponse;
use App\Http\Resources\CustomerCollection;
use App\Http\Resources\MasterData\MasterDataBrandMaterialPaperCollection;
// use App\Http\Resources\MasterData\MasterDataCustomerCollection;
use App\Http\Resources\MasterData\MasterDataJobOrderCategoryCollection;
use App\Http\Resources\MasterData\MasterDataMaterialPaperCollection;
use App\Http\Resources\MasterData\MasterDataPaymentMethodCollection;
use App\Http\Resources\MasterData\MasterDataProductCategoryCollection;
use App\Models\BrandMaterialPaper;
use App\Models\Customer;
use App\Models\JobOrderCategory;
use App\Models\MaterialPaper;
use App\Models\PaymentMethod;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class MasterData
{

  const JOB_ORDER_CATEGORIES = 'job_order_categories';
  const MATERIAL_PAPERS = 'material_papers';
  const BRAND_MATERIAL_PAPER = 'brands_material_paper';
  const CUSTOMERS = 'customers';
  const PAYMENT_METHOD = "payment_method";
  const PRODUCT_CATEGORIES = "product_categories";


  public function handle(Request $request)
  {

    /**
     * Cannot use $request->only()
     * cause the request doesn't have object key in root.
     */
    $data = $request->all();
    $sources = [];

    if (is_array($data)) {
      foreach ($data as $key => $value) {
        // create array empty for all source data.
        $sources = array_merge($sources, [$value['objectName'] => array()]);
        $sourceData = $this->getData($value['objectName']);
        $sources = array_merge(
          $sources,
          [$value['objectName'] => $sourceData]
        );
      }
    }
    return $sources;
  }

  /**
   * Method to get source model data.
   */
  private function getData(string $objectName)
  {
    $objectSource = [];

    switch ($objectName) {
      case self::JOB_ORDER_CATEGORIES:
        $objectSource = new MasterDataJobOrderCategoryCollection(JobOrderCategory::all());
        break;
      case self::CUSTOMERS:
        // $objectSource = new MasterDataCustomerCollection(Customer::all());
        // use customer collection intread master data customer. Cause front-end need to get information about customers.
        $objectSource = new CustomerCollection(Customer::all());
        break;
      case self::BRAND_MATERIAL_PAPER:
        $objectSource = new MasterDataBrandMaterialPaperCollection(BrandMaterialPaper::all());
        break;
      case self::MATERIAL_PAPERS:
        $objectSource = new MasterDataMaterialPaperCollection(MaterialPaper::all());
        break;
      case self::PAYMENT_METHOD:
        $objectSource = new MasterDataPaymentMethodCollection(PaymentMethod::all());
        break;
      case self::PRODUCT_CATEGORIES:
        $objectSource = new MasterDataProductCategoryCollection(ProductCategory::all());
        break;
      default:
        $objectSource = [];
        break;
    }

    return  $objectSource;
  }
}
