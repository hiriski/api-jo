<?php

namespace App\Services;

use App\Models\Customer;
use App\Models\JobOrder;
use App\Models\Role;
use App\UploadImage;
use Carbon\Carbon;

class JobOrderService
{

  /**
   * Find all job orders.
   */
  public function findAll($data)
  {

    $jobOrders = null;
    $per_page = isset($data['per_page']) ? (int) $data['per_page'] : 24;

    switch (auth()->user()->role->id) {
      case Role::ROLE_ID_CASHIER:
        $jobOrders = JobOrder::orderBy('created_at', 'desc')
          ->active()->with(['invoices'])->paginate($per_page);
        break;

      case Role::ROLE_ID_PRODUCTION_TEAM:
        $jobOrders = JobOrder::active()->readyToProduction()
          ->orderBy('created_at', 'desc')->paginate($per_page);
        break;

      default:
        $jobOrders = JobOrder::active()->orderBy('created_at', 'desc')
          ->paginate($per_page);
        break;
    }

    return  $jobOrders;
  }

  /**
   * Search job order by keywords.
   */
  public function searchByKeyword($data, $searchTerm)
  {
    $jobOrders = null;
    $per_page = isset($data['per_page']) ? (int) $data['per_page'] : 24;


    switch (auth()->user()->role->id) {
      case Role::ROLE_ID_CASHIER:
        $jobOrders = JobOrder::orderBy('created_at', 'desc')
          ->active()->with(['invoices']);
        break;

      case Role::ROLE_ID_PRODUCTION_TEAM:
        $jobOrders = JobOrder::active()->readyToProduction()
          ->orderBy('created_at', 'desc');
        break;

      default:
        $jobOrders = JobOrder::active()->orderBy('created_at', 'desc');
        break;
    }

    // Query search.
    $jobOrders = $jobOrders->where(function ($q) use ($searchTerm) {
      $q->where('title', 'LIKE', '%' . $searchTerm . '%');
      $q->orWhere('order_number', 'LIKE', '%' . $searchTerm . '%');
    })->paginate($per_page);

    return $jobOrders;
  }

  /**
   * Find by id
   */
  public function findById(string $id)
  {
    return JobOrder::findOrFail($id);
  }


  /**
   * Create job order.
   */
  public function create($data, CustomerService $customerService)
  {
    $userId = auth()->id();
    $customer = null;
    $newJobOrder = $data;
    $jobOrderImagesDirname   = 'job-orders/' . Carbon::now()->format('Y-m-d');

    // Create customer data.
    if (isset($data['customer']) && !isset($data['customer_id'])) {
      $customer = $customerService->create($data['customer']);
      $newJobOrder['customer_id'] = $customer->id;
    }

    // Create Job Order.
    $newJo = JobOrder::create($newJobOrder);

    // Create notes.
    if (isset($data['notes'])) {
      $newJo->notes()->create([
        'user_id'   => $userId,
        'body'      => $data['notes'],
      ]);
    }

    // Create images.
    if (isset($data['images'])) {
      if (count($data['images'])) {
        foreach ($data['images'] as $jobOrderImage) {
          $image = UploadImage::handle($jobOrderImage, $jobOrderImagesDirname);
          $newJo->images()->create(
            array_merge($image, ['user_id' => $userId])
          );
        }
      }
    }

    return $newJo->fresh();
  }

  public function update($data, CustomerService $customerService, string $id)
  {
    $userId = auth()->id();
    $customer = null;
    $jobOrderData = $data;
    $jobOrder = JobOrder::findOrFail($id);
    $jobOrderImagesDirname   = 'job-orders/' . Carbon::now()->format('Y-m-d');

    // Remove customer relation.
    if (!isset($data['customer']) && !isset($data['customer_id'])) {
      $jobOrderData['customer_id'] = null;
    }

    // Create customer relation.
    if (isset($data['customer']) && !isset($data['customer_id'])) {
      $customer = $customerService->create($data['customer']);
      $jobOrderData['customer_id'] = $customer->id;
    }

    // Remove notes relation.
    if (!isset($data['notes_ids'])) {
      $jobOrder->notes()->delete();
    }

    // Remove images relation.
    if (!isset($data['image_ids'])) {
      $jobOrder->images()->delete();
    }

    // Create notes.
    if (isset($data['notes'])) {
      $jobOrder->notes()->create([
        'user_id'   => $userId,
        'body'      => $data['notes'],
      ]);
    }

    // Create Job Order.
    $jobOrder->update($jobOrderData);

    // Create images.
    if (isset($data['images'])) {
      if (count($data['images'])) {
        foreach ($data['images'] as $jobOrderImage) {
          $image = UploadImage::handle($jobOrderImage, $jobOrderImagesDirname);
          $jobOrder->images()->create(
            array_merge($image, ['user_id' => $userId])
          );
        }
      }
    }

    return $jobOrder->fresh();
  }


  /**
   * Delete job order.
   */
  public function delete(string $id)
  {
    $jobOrder = JobOrder::findOrFail($id);
    return $jobOrder->delete();
  }
}
