<?php

namespace App\Services;

use App\Http\Requests\Warehouse\StoreMaterialRequest;
use App\Models\Material;
use App\UploadImage;
use Illuminate\Http\Request;

class MaterialService
{

  /**
   * Find all materials.
   */
  public function findAll()
  {
    $materials = Material::isActive()->orderBy('created_at', 'desc')->with(['brand', 'category'])->get();
    return  $materials;
  }

  /**
   * Search product by keywords.
   */
  public function searchByKeyword($searchTerm)
  {
    $materials = Material::isActive()->orderBy('created_at', 'desc')->with(['brand', 'category']);

    // Query search.
    return $materials->where(function ($q) use ($searchTerm) {
      $q->where('name', 'LIKE', '%' . $searchTerm . '%');
      $q->orWhere('description', 'LIKE', '%' . $searchTerm . '%');
    })->get();
  }

  /**
   * Find by id
   */
  public function findById(string $id)
  {
    return Material::with(['brand', 'category'])->findOrFail($id);
  }


  /**
   * Create product.
   */
  public function create(StoreMaterialRequest $request)
  {
    $data = $request->only([
      'name',
      'unit',
      'display_unit',
      'material_brand_id',
      'material_category_id',
      'stock_length',
      'stock_width',
      'stock_pcs',
      'width_per_roll',
      'length_per_roll',
      'price',
      'description',
      'status',
      'image'
    ]);

    $image_url = null;

    if ($request->has('image')) {
      $image_url  = UploadImage::handle($data['image'], 'materials', false);
    }

    $newMaterial = Material::create(array_merge(
      $data,
      ['image_url' => $image_url]
    ));

    // return fresh new material
    return Material::with(['brand', 'category'])->findOrFail($newMaterial->id);
  }

  /**
   * Update product..
   */
  public function update(Request $request, string $id)
  {
    $data = $request->only([
      'name',
      'unit',
      'display_unit',
      'material_brand_id',
      'material_category_id',
      'stock_length',
      'stock_width',
      'stock_pcs',
      'width_per_roll',
      'length_per_roll',
      'price',
      'description',
      'status',
      'new_image'
    ]);

    // Find product by id.
    $material = Material::findOrFail($id);

    // Image.
    $image_url = $material->image_url;

    if ($request->has('new_image')) {
      $image_url  = UploadImage::handle($data['new_image'], 'materials', false);
    }

    $material->update(array_merge(
      $data,
      ['image_url' => $image_url]
    ));

    return $material->fresh();
  }

  /**
   * Create product.
   */
  public function delete(string $id)
  {
    return Material::destroy($id);
  }
}
