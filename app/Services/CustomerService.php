<?php

namespace App\Services;

use App\Models\Customer;
use App\UploadImage;

class CustomerService
{

  /**
   * Find all customers.
   */
  public function findAll($data)
  {
    $per_page = isset($data['per_page']) ? (int) $data['per_page'] : 24;
    $customers = Customer::orderBy('created_at', 'desc')->paginate($per_page);
    return  $customers;
  }

  /**
   * Search customer by keywords.
   */
  public function searchByKeyword($data, $searchTerm)
  {
    $per_page = isset($data['per_page']) ? (int) $data['per_page'] : 24;
    $customers = Customer::orderBy('created_at', 'desc');

    // Query search.
    return $customers->where(function ($q) use ($searchTerm) {
      $q->where('name', 'LIKE', '%' . $searchTerm . '%');
      $q->orWhere('notes', 'LIKE', '%' . $searchTerm . '%');
    })->paginate($per_page);
  }

  /**
   * Find by id
   */
  public function findById(string $id)
  {
    return Customer::findOrFail($id);
  }


  /**
   * Create customer.
   */
  public function create(array $data)
  {
    $photo_url = null;

    if (isset($data['photo'])) {
      $photo_url  = UploadImage::handle($data['photo'], 'customers', false);
    }

    $newCustomer = Customer::create(array_merge(
      $data,
      ['photo_url' => $photo_url]
    ));

    $dataAddresses = isset($data['addresses']) ? $data['addresses'] : [];

    if (count($dataAddresses)) {
      foreach ($dataAddresses as $address) {
        $this->createCustomerAddress($newCustomer, $address);
      }
    }

    // return fresh new Customer
    return Customer::findOrFail($newCustomer->id);
  }

  /**
   * Create customer address.
   */
  private function createCustomerAddress(Customer $customer, array $data)
  {
    $customer->addresses()->create([
      'label'             => $data['label'],
      'address_details'   => $data['address_details'],
      'district'          => $data['district'],
      'city'              => $data['city'],
      'country'           => $data['country'],
      'province'          => $data['province'],
      'city'              => $data['city'],
    ]);
  }

  /**
   * Update product..
   */
  public function update(array $data, string $id)
  {

    // Find customer by id.
    $customer = Customer::findOrFail($id);

    // Request data address.
    $dataAddresses = isset($data['addresses']) ? $data['addresses'] : [];

    // Current customer photo.
    $photo_url = $customer->photo_url;

    if (isset($data['new_photo'])) {
      $photo_url  = UploadImage::handle($data['new_photo'], 'customers', false);
    }

    $customer->update(array_merge(
      $data,
      ['photo_url' => $photo_url]
    ));

    // Delete customer address.
    $customer->addresses()->delete();

    // Re-create customer address.
    if (count($dataAddresses)) {
      foreach ($dataAddresses as $address) {
        $this->createCustomerAddress($customer, $address);
      }
    }

    return $customer->fresh();
  }

  /**
   * Delete customer.
   */
  public function delete(string $id)
  {
    $customer = Customer::findOrFail($id);
    return $customer->delete();
  }

  public function deleteAddress(array $data)
  {
    if (isset($data['address_ids']) && isset($data['customer_id'])) {
      $customer = Customer::findOrFail($data['customer_id']);
      return $customer->addresses()->detach($data['address_ids']);
    }
  }
}
