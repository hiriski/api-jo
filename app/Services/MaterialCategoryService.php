<?php

namespace App\Services;

use App\Http\Requests\Warehouse\StoreMaterialCategoryRequest;
use App\Models\MaterialCategory;
use App\UploadImage;
use Illuminate\Http\Request;

class MaterialCategoryService
{

  /**
   * Find all materials.
   */
  public function findAll()
  {
    $materialCategories = MaterialCategory::orderBy('name', 'desc')->with(['materials'])->get();
    return  $materialCategories;
  }

  /**
   * Search product by keywords.
   */
  public function searchByKeyword($searchTerm)
  {
    $materialCategories = MaterialCategory::orderBy('name', 'desc')->with(['materials']);

    // Query search.
    return $materialCategories->where(function ($q) use ($searchTerm) {
      $q->where('name', 'LIKE', '%' . $searchTerm . '%');
      $q->orWhere('description', 'LIKE', '%' . $searchTerm . '%');
    })->get();
  }

  /**
   * Find by id
   */
  public function findById(string $id)
  {
    return MaterialCategory::with(['materials'])->findOrFail($id);
  }


  /**
   * Create product.
   */
  public function create(StoreMaterialCategoryRequest $request)
  {
    $data = $request->only([
      'name',
      'unit_material',
      'description',
      'image'
    ]);

    $image_url = null;

    if ($request->has('image')) {
      $image_url  = UploadImage::handle($data['image'], 'materials/categories', false);
    }

    $newMaterial = MaterialCategory::create(array_merge(
      $data,
      ['image_url' => $image_url]
    ));

    // return fresh new material
    return MaterialCategory::findOrFail($newMaterial->id);
  }

  /**
   * Update product..
   */
  public function update(Request $request, string $id)
  {
    $data = $request->only([
      'name',
      'material_unit',
      'description',
      'new_image'
    ]);

    // Find product by id.
    $materialCategory = MaterialCategory::findOrFail($id);

    // Image.
    $image_url = $materialCategory->image_url;

    if ($request->has('new_image')) {
      $image_url  = UploadImage::handle($data['new_image'], 'materials/categories', false);
    }

    $materialCategory->update(array_merge(
      $data,
      ['image_url' => $image_url]
    ));

    return $materialCategory->fresh();
  }

  /**
   * Create product.
   */
  public function delete(string $id)
  {
    return MaterialCategory::destroy($id);
  }
}
