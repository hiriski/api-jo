<?php

namespace App\Services;

use App\Http\Requests\StoreProductRequest;
use App\Models\Product;
use App\Models\Image as ModelImage;
use App\UploadImage;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductService
{

  /**
   * Find all product
   */
  public function findAll($request)
  {
    $products = null;
    if (!empty($request->limit)) {
      $products = Product::orderBy('created_at', 'desc')->paginate($request->limit);
    } else {
      $products = Product::orderBy('created_at', 'desc')->paginate(12);
    }

    return $products;
  }

  /**
   * Search product by keywords.
   */
  public function searchByKeyword($request, $searchTerm)
  {
    $products = Product::where('status', 'active');
    $products = $products->where(function ($q) use ($searchTerm) {
      $q->where('title', 'LIKE', '%' . $searchTerm . '%');
      $q->orWhere('description', 'LIKE', '%' . $searchTerm . '%');
    });

    if (!empty($request->limit)) {
      $products = $products->paginate($request->limit);
    } else {
      $products = $products->paginate(12);
    }

    return $products;
  }

  /**
   * Find by id
   */
  public function findById(string $id)
  {
    return Product::findOrFail($id);
  }


  /**
   * Create product.
   */
  public function create(StoreProductRequest $request)
  {
    $data = $request->merge(['user_id' => auth()->id()])->only([
      'title',
      'user_id',
      'category_id',
      'slug',
      'images',
      'description',
      'body',
      'price',
      'status'
    ]);
    $product = Product::create($data);

    if ($request->has('images')) {
      foreach ($data['images'] as $requestImage) {
        $image  = UploadImage::handle($requestImage);
        $product->images()->create($image);
      }
    }

    return $product->fresh();
  }

  /**
   * Update product..
   */
  public function update(StoreProductRequest $request, string $id)
  {
    $data = $request->only([
      'title',
      'category_id',
      'slug',
      'images',
      'description',
      'body',
      'price',
      'status'
    ]);

    // Find product by id.
    $product = Product::findOrFail($id);

    // Delete product images.
    $product->images()->delete();

    // Re-create images relations.
    if ($request->has('images')) {
      foreach ($data['images'] as $requestImage) {
        $image  = UploadImage::handle($requestImage);
        $product->images()->create($image);
      }
    }

    // Update product.
    $product->update($data);

    return $product->fresh();
  }

  /**
   * Create product.
   */
  public function delete(string $id)
  {
    return Product::destroy($id);
  }
}
