<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Notes extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'            => $this->id,
            'body'          => $this->body,
            'user'          => new UserShort($this->whenLoaded('user')),
            'created_at'    => $this->created_at,
        ];
    }

    public static $wrap = null;
}
