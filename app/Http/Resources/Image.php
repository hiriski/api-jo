<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class Image extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        // $directoryName = Carbon::parse($this->created_at)->format('Y-m-d');
        // $path = URL::to('/storage/images/') . '/' . $directoryName . '/';

        // return parent::toArray($request);
        return [
            'id'                => $this->id,
            'image_xs'          => $this->image_xs ? URL::to('/storage/images/') . $this->image_xs : null,
            'image_sm'          => $this->image_sm ? URL::to('/storage/images/') . $this->image_sm : null,
            'image_md'          => $this->image_md ? URL::to('/storage/images/') . $this->image_md : null,
            'image_lg'          => $this->image_lg ? URL::to('/storage/images/') . $this->image_lg : null,
            'image_original'    => $this->image_original ? URL::to('/storage/images/') . $this->image_original : null,
            'image_xs'          => $this->image_xs ? URL::to('/storage/images/') . $this->image_xs : null,
            'created_at'        => $this->created_at,
        ];
    }

    public static $wrap = null;
}
