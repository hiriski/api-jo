<?php

namespace App\Http\Resources\Warehouse;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;

class Material extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'unit'              => $this->unit,
            'display_unit'      => $this->display_unit,
            'stock_length'      => $this->stock_length,
            'stock_width'       => $this->stock_width,
            'stock_pcs'         => $this->stock_pcs,
            'width_per_roll'    => $this->width_per_roll,
            'length_per_roll'   => $this->length_per_roll,
            'price'             => $this->price,
            'description'       => $this->description,
            'status'            => $this->status,
            'image_url'         => $this->image_url ? URL::to('/storage/images/') . $this->image_url : null,
            'created_at'        => $this->created_at,
            'brand'             => new MaterialBrand($this->whenLoaded('brand')),
            'category'          => new MaterialCategory($this->whenLoaded('category')),
        ];
    }
}
