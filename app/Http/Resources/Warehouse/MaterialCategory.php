<?php

namespace App\Http\Resources\Warehouse;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;

class MaterialCategory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'unit_material'     => $this->unit_material,
            'description'       => $this->description,
            'material_counts'   => $this->material_counts,
            'image_url'         => $this->image_url ? URL::to('/storage/images/') . $this->image_url : null,
            'materials'         => new MaterialCollection($this->whenLoaded('materials')),
        ];
    }
}
