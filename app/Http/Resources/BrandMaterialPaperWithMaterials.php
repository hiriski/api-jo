<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BrandMaterialPaperWithMaterials extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'available_sizes'   => $this->available_sizes,
            'material_counts'   => $this->material_counts,
            'materials'         => new MaterialPaperCollection($this->whenLoaded('material_papers'))
        ];
    }
}
