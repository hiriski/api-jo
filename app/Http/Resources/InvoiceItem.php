<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'description'   => $this->description,
            'notes'         => $this->notes,
            'quantity'      => $this->quantity,
            'price'         => $this->price,
            'discount'      => $this->discount,
            'total'         => $this->total,
        ];
    }
}
