<?php

namespace App\Http\Resources;

use App\Http\Resources\Warehouse\MaterialCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class JobOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'                    => $this->id,
            'created_by'            => new UserShort($this->whenLoaded('created_by')),
            'updated_by'            => new UserShort($this->whenLoaded('updated_by')),
            'product'               => new Product($this->whenLoaded('product')),
            'customer'              => new Customer($this->whenLoaded('customer')),
            'status'                => new JobOrderStatus($this->whenLoaded('status')),
            'category'              => new JobOrderCategory($this->whenLoaded('category')),
            'payment_status'        => new PaymentStatus($this->whenLoaded('payment_status')),
            'payment_method'        => new PaymentMethod($this->whenLoaded('payment_method')),
            'bank_account'          => new BankAccount($this->whenLoaded('bank_account')),
            'production_status'     => new JobOrderProductionStatus($this->whenLoaded('production_status')),
            'is_ready_to_production' => $this->is_ready_to_production,
            'title'                 => $this->title,
            'order_number'          => $this->order_number,
            'order_date'            => $this->order_date,
            'due_date'              => $this->due_date,
            'body'                  => $this->body,
            'down_payment'          => $this->down_payment,
            'dimension_material_length'  => $this->dimension_material_length,
            'dimension_material_width'   => $this->dimension_material_width,
            'quantity'              => $this->quantity,
            'price'                 => $this->price,
            'tax'                    => $this->tax,
            'additional_price_name'    => $this->additional_price_name,
            'additional_price'      => $this->additional_price,
            'rejected_by'           => new UserShort($this->whenLoaded('rejected_by')),
            'reason_rejected'       => $this->reason_rejected,
            'is_approved'           => $this->is_approved,
            'percentage_progress'   => $this->percentage_progress,
            'progress_by'           => new UserShort($this->whenLoaded('progress_by')),
            'notes'                 => new NotesCollection($this->whenLoaded('notes')),
            'attachments'           => new AttachmentCollection($this->whenLoaded('attachments')),
            'invoices'              => new InvoiceCollection($this->whenLoaded('invoices')),
            'materials'             => new MaterialCollection($this->whenLoaded('materials')),
            'machines'              => new MachineCollection($this->whenLoaded('machines')),
            'images'                => new ImageCollection($this->whenLoaded('images')),
            'comments'              => new CommentCollection($this->whenLoaded('comments')),
            'total'                 => $this->total,
            'created_at'            => $this->created_at,
            'updated_at'            => $this->updated_at,
        ];
    }

    public static $wrap = null;
}
