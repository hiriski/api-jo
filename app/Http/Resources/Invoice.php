<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Invoice extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'items'                 => new InvoiceItemCollection($this->whenLoaded('items')),
            'invoice_number'        => $this->invoice_number,
            'bill_to_name'          => $this->bill_to_name,
            'bill_to_phone_number'  => $this->bill_to_phone_number,
            'bill_to_address'       => $this->bill_to_address,
            'invoice_date'          => $this->invoice_date,
            'invoice_due_date'      => $this->invoice_due_date,
            'tax'                   => $this->tax,
            'bill_discount'         => $this->bill_discount,
            'notes'                 => $this->notes,
            'terms_and_conditions'  => $this->terms_and_conditions,
            'notes'                 => $this->notes,
        ];
    }

    public static $wrap = null;
}
