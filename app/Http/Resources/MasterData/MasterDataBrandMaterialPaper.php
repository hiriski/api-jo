<?php

namespace App\Http\Resources\MasterData;

use Illuminate\Http\Resources\Json\JsonResource;

class MasterDataBrandMaterialPaper extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'brand_id'  => $this->brand_id,
            'name'      => $this->name,
        ];
    }

    public static $wrap = null;
}
