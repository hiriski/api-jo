<?php

namespace App\Http\Resources\MasterData;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MasterDataBrandMaterialPaperCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public static $wrap = null;
}
