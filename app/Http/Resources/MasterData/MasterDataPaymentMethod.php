<?php

namespace App\Http\Resources\MasterData;

use App\Http\Resources\PaymentMethod;
// use Illuminate\Http\Resources\Json\JsonResource;

class MasterDataPaymentMethod extends PaymentMethod
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
