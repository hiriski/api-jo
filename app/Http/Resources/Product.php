<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'            => $this->id,
            'slug'          => $this->slug,
            'title'         => $this->title,
            'description'   => $this->description,
            'body'          => $this->body,
            'status'        => $this->status,
            'category'      => new ProductCategory($this->whenLoaded('category')),
            'tags'          => new TagCollection($this->whenLoaded('tags')),
            'images'        => new ImageCollection($this->whenLoaded('images')),
        ];
    }

    public static $wrap = null;
}
