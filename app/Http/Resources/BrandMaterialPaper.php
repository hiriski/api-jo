<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BrandMaterialPaper extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'available_sizes'   => $this->available_sizes,
            'notes'             => $this->notes,
            'material_counts'   => $this->material_counts,
        ];
    }

    public static $wrap = null;
}
