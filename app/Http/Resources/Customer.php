<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;

class Customer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'phone_number'  => $this->phone_number,
            'email'         => $this->email,
            'photo_url'     => $this->photo_url ? URL::to('/storage/images/') . $this->photo_url : null,
            'address'       => new AddressCollection($this->whenLoaded('addresses')),
            'notes'         => $this->notes,
        ];
    }

    public static $wrap = null;
}
