<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJobOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                     => ['required', 'string', 'min:3', 'max:255'],
            'category_id'               => ['required', 'integer', 'exists:job_order_categories,id'],
            'customer_id'               => ['nullable', 'string', 'exists:customers,id'],
            'payment_status_id'         => ['required', 'integer', 'exists:payment_statuses,id'],
            'payment_method_id'         => ['nullable', 'exists:payment_statuses,id'],
            'bank_account_id'           => ['nullable', 'exists:bank_accounts,id'],
            'is_ready_to_production'    => ['nullable', 'boolean'],
            'order_date'                => ['required', 'date'],
            'due_date'                  => ['required', 'date'],
            'down_payment'              => ['nullable', 'integer'],
            'dimension_material_length' => ['required', 'integer'],
            'dimension_material_width'  => ['required', 'integer'],
            'quantity'                  => ['required', 'integer'],
            'price'                     => ['nullable', 'integer'],
            'additional_price_name'     => ['nullable', 'string', 'min:1', 'max:255'],
            'additional_price'          => ['nullable', 'integer'],
            'body'                      => ['nullable',  'string', 'min:3', 'max:2000'],
            'notes'                     => ['nullable',  'string', 'min:3', 'max:2000'],
            'images'                    => ['nullable', 'array'],
            'images.*'                  => ['nullable', 'string'],
            'customer'                  => ['nullable'],
            'customer.addresses'        => ['nullable', 'array'],
        ];
    }
}
