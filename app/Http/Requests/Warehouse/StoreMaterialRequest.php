<?php

namespace App\Http\Requests\Warehouse;

use Illuminate\Foundation\Http\FormRequest;

class StoreMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => ['string', 'min:3', 'max:255', 'required'],
            'material_category_id'  => ['required', 'integer', 'exists:material_categories,id'],
            'material_brand_id'     => ['required', 'integer', 'exists:material_brands,id'],
            'unit'                  => ['required', 'in:mm,cm,m,inch,liter,kg,pcs,sheet,ream,roll'],
            'display_unit'          => ['required', 'in:mm,cm,m,inch,liter,kg,pcs,sheet,ream,roll'],
            'status'                => ['in:active,inactive'],
            'description'           => ['string', 'nullable', 'min:3', 'max:255'],
        ];
    }
}
