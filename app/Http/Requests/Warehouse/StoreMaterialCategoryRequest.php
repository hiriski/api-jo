<?php

namespace App\Http\Requests\Warehouse;

use Illuminate\Foundation\Http\FormRequest;

class StoreMaterialCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => ['string', 'min:3', 'max:255', 'required'],
            'unit_material'         => ['required', 'in:mm,cm,m,inch,liter,kg,pcs,sheet,ream,roll'],
            'description'           => ['string', 'nullable', 'min:3', 'max:255'],
        ];
    }
}
