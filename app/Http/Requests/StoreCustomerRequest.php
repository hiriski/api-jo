<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => ['required', 'string', 'min:3', 'max:255'],
            'customer_type' => ['string', 'nullable', 'in:individual,company'],
            'phone_number'  => ['string', 'nullable', 'min:8', 'max:255'],
            'email'         => ['string', 'nullable', 'min:3', 'max:255'],
            'notes'         => ['string', 'nullable', 'min:3', 'max:255'],
            'addresses'     => ['array', 'nullable'],
        ];
    }
}
