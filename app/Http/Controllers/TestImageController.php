<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Models\Image as ModelImage;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class TestImageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $prefixDateFolder   = Carbon::now()->format('Y-m-d');
        $destinationPath    = storage_path(ModelImage::DESTINATION_PATH . $prefixDateFolder . '/');
        $fileName           = Str::random(7) . time();

        if (!File::isDirectory($destinationPath)) {
            File::makeDirectory($destinationPath, 0777, true, true);
        }

        try {
            if ($requestImage = $request->image) {
                $img = Image::make($requestImage);

                foreach (ModelImage::IMAGE_SIZES as $key => $value) {
                    if ($key !== 'original')
                        $img->resize($value, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    // ->insert(storage_path('/app/public/logo200x200.png'), 'bottom-right', 30, 30);
                    $img->save(
                        $destinationPath . $fileName . '_' . $key . '.' . ModelImage::IMAGE_EXTENSION,
                        100,
                    );
                }

                return response()->json([
                    'message'   => 'Success!'
                ], JsonResponse::HTTP_CREATED);
                // return $img->response('jpg');
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
