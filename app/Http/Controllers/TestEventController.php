<?php

namespace App\Http\Controllers;

use App\Events\TestEvent;
use Illuminate\Http\Request;

class TestEventController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        event(new TestEvent([
            'id'        => time(),
            'title'     => "Hi, this is notification from server!",
            'message'   => 'Hi there, this is message from server',
            'bigText'   => "In the above case, you're navigating to the Media screen, which is in a navigator nested inside the Sound screen, which is in a navigator nested inside the Settings screen."
        ]));
        // try {
        // } catch (\Exception $exception) {
        //     return response()->json([
        //         'message'   => $exception->getMessage()
        //     ]);
        // }
    }
}
