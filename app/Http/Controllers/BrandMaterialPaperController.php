<?php

namespace App\Http\Controllers;

use App\Http\Resources\BrandMaterialPaper as ResourcesBrandMaterialPaper;
use App\Http\Resources\BrandMaterialPaperCollection;
use App\Models\BrandMaterialPaper;
use Illuminate\Http\Request;

class BrandMaterialPaperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = BrandMaterialPaper::all();
        return new BrandMaterialPaperCollection($brands);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only(['name', 'icon', 'available_sizes', 'notes']);
        $brandMaterialPaper = BrandMaterialPaper::create($data);
        return new ResourcesBrandMaterialPaper($brandMaterialPaper);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = BrandMaterialPaper::findOrFail($id);
        return new ResourcesBrandMaterialPaper($brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = BrandMaterialPaper::findOrFail($id);
        $data = $request->only(['name', 'icon', 'available_sizes']);
        $brand->update($data);
        return new ResourcesBrandMaterialPaper($brand);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = BrandMaterialPaper::findOrfail($id);
        $brand->delete();
    }
}
