<?php

namespace App\Http\Controllers;

use App\Services\MasterData;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MasterDataController extends Controller
{


    /**
     * @var MasterData
     */
    private $masterData;

    public function __construct(MasterData $masterData)
    {
        $this->masterData = $masterData;
    }


    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        try {
            $result = $this->masterData->handle($request);
            return response()->json($result);
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage()
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
