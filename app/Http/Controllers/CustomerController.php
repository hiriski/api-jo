<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteCustomerAddressRequest;
use App\Http\Requests\StoreCustomerRequest;
use App\Http\Resources\Customer as CustomerResource;
use App\Http\Resources\CustomerCollection;
use App\Services\CustomerService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    private $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customers = $this->customerService->findAll($request->all());
        return new CustomerCollection($customers);
    }

    /**
     * Search material
     */
    public function search(Request $request, $searchTerm)
    {
        $customers = $this->customerService->searchByKeyword($request->all(), $searchTerm);
        return new CustomerCollection($customers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCustomerRequest $request)
    {
        $data = $request->only([
            'name',
            'customer_type',
            'phone_number',
            'email',
            'addresses',
            'photo',
            'notes'
        ]);
        try {
            $customer = $this->customerService->create($data);
            return response()->json(
                new CustomerResource($customer),
                JsonResponse::HTTP_CREATED
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = $this->customerService->findById($id);
        return new CustomerResource($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only([
            'name',
            'customer_type',
            'phone_number',
            'email',
            'addresses',
            'new_photo',
            'notes'
        ]);
        try {
            $customer = $this->customerService->update($data, $id);
            return response()->json(
                new CustomerResource($customer),
                JsonResponse::HTTP_OK
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $result = $this->customerService->delete($id);
            if ($result) return response()->json([
                'message'   => 'Customer has been delete!'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }


    /**
     * Delete customer address
     */
    public function deleteAddress(DeleteCustomerAddressRequest $request)
    {
        $result = $this->customerService->deleteAddress($request->only(['customer_id', 'address_ids']));
        if ($result) {
            return response()->json([
                'message'   => 'Customer address has been delete!'
            ]);
        } else {
            return response()->json([
                'message'   => 'Failed to delete customer address'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
