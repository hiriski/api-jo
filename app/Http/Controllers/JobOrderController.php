<?php

namespace App\Http\Controllers;

use App\Http\Resources\CustomerCollection;
use App\Http\Resources\JobOrder as ResourcesJobOrder;
use App\Http\Resources\JobOrderCollection;
use App\Models\Customer;
use App\Models\JobOrder;
use App\Models\JobOrderProductionStatus;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\JobOrderStatus;


// events
use App\Events\JobOrderCreated;
// use App\Events\TestEvent;
use App\Http\Requests\StoreJobOrderRequest;

// services.
use App\Services\JobOrderService;
use App\Services\CustomerService;

class JobOrderController extends Controller
{


    private $jobOrderService;
    private $customerService;

    public function __construct(JobOrderService $jobOrderService, CustomerService $customerService)
    {
        $this->jobOrderService = $jobOrderService;
        $this->customerService = $customerService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jobOrders = $this->jobOrderService->findAll($request->all());
        return new JobOrderCollection($jobOrders);
    }

    /**
     * Search job order
     */
    public function search(Request $request, $searchTerm)
    {
        $jobOrders = $this->jobOrderService->searchByKeyword($request, $searchTerm);
        return new JobOrderCollection($jobOrders);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(StoreJobOrderRequest $request)
    {
        $data = $request->merge([
            'created_by_user_id' => auth()->id()
        ])->only([
            'created_by_user_id',
            'product_id',
            'customer_id',
            'category_id',
            'payment_status_id',
            'payment_method_id',
            'bank_account_id',
            'is_ready_to_production',
            'title',
            'order_number',
            'order_date',
            'due_date',
            'body',
            'down_payment',
            'dimension_material_length',
            'dimension_material_width',
            'quantity',
            'price',
            'tax',
            'additional_price_name',
            'additional_price',
            'notes',
            'customer',
            'images'
        ]);

        try {

            $newJobOrder = $this->jobOrderService->create($data, $this->customerService);

            /**
             * @var $notificationData
             */
            $notificationData = [
                'title'     => 'Ada Job Order Baru!',
                'summary'   => $newJobOrder->title . $newJobOrder->body,
                'priority'  => 1,
                'data'      => new ResourcesJobOrder($newJobOrder->fresh())
            ];

            // Dispatch event
            event(new JobOrderCreated($notificationData));

            return response()->json(
                new ResourcesJobOrder($newJobOrder->fresh()),
                JsonResponse::HTTP_CREATED
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobOrder = $this->jobOrderService->findById($id);
        return new ResourcesJobOrder($jobOrder);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->merge([
            'updated_by_user_id' => auth()->id()
        ])->only([
            'updated_by_user_id',
            'product_id',
            'customer_id',
            'category_id',
            'payment_status_id',
            'payment_method_id',
            'bank_account_id',
            'is_ready_to_production',
            'title',
            'order_number',
            'order_date',
            'due_date',
            'body',
            'down_payment',
            'dimension_material_length',
            'dimension_material_width',
            'quantity',
            'price',
            'tax',
            'additional_price_name',
            'additional_price',
            'notes',
            'customer',
            'images',
            'notes_ids',
            'image_ids'
        ]);

        try {

            $jobOrder = $this->jobOrderService->update($data, $this->customerService, $id);

            /**
             * @var $notificationData
             */
            // $notificationData = [
            //     'title'     => 'Ada Job Order Baru!',
            //     'summary'   => $jobOrder->title . $jobOrder->body,
            //     'priority'  => 1,
            //     'data'      => new ResourcesJobOrder($jobOrder->fresh())
            // ];

            // Dispatch event
            // event(new JobOrderCreated($notificationData));

            return response()->json(
                new ResourcesJobOrder($jobOrder->fresh()),
                JsonResponse::HTTP_CREATED
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $result = $this->jobOrderService->delete($id);
            if ($result) return response()->json([
                'message'   => 'Job Order has been delete!'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }


    /**
     * Download invoice pdf.
     */
    public function downloadInvoice(Request $request, $id)
    {
        $data = JobOrder::findOrFail($id);
        dd($data);
    }



    /**
     * Update production status jo.
     */
    public function updateProductionStatus(Request $request, $id)
    {
        $jobOrder = JobOrder::findOrFail($id);
        $status = (int) $request->production_status_id;
        $progressPercentage = (int) $status === JobOrderProductionStatus::DONE ? 100 : $jobOrder->percentage_progress;
        try {
            if ($status) {
                $jobOrder->update([
                    'production_status_id'  => (int) $status,
                    'percentage_progress'   => (int) $progressPercentage, // also update percentage progress.
                ]);
                return new ResourcesJobOrder($jobOrder->fresh());
            } else {
                return response()->json([
                    'message'   => "production_status_id cannot be empty / null"
                ], JsonResponse::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage()
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update percentage progress jo.
     */
    public function updatePercentageProgress(Request $request, $id)
    {
        $jobOrder = JobOrder::findOrFail($id);
        try {
            if ($request->value) {
                $jobOrder->update([
                    'value' => (int) $request->value
                ]);
                return new ResourcesJobOrder($jobOrder->fresh());
            } else {
                return response()->json([
                    'message'   => "value cannot be empty / null"
                ], JsonResponse::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage()
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Add notes to jo.
     */
    public function addNote(Request $request, $id)
    {
        $jobOrder = JobOrder::findOrFail($id);
        try {
            if ($request->notes) {
                $jobOrder->notes()->create([
                    'user_id'   => auth()->id(),
                    'body'      => $request->notes,
                ]);
                return new ResourcesJobOrder($jobOrder->fresh());
            } else {
                return response()->json([
                    'message'   => "Notes cannot be empty / null"
                ], JsonResponse::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage()
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Reject jo.
     */
    public function reject(Request $request, $id)
    {
        $jobOrder = JobOrder::findOrFail($id);
        try {
            if ($request->reason) {
                $jobOrder->update([
                    'rejected_by_user_id'   => auth()->id(),
                    'reason_rejected'       => (string) $request->reason,
                    'status_id'             => JobOrderStatus::REJECTED,
                ]);
                return new ResourcesJobOrder($jobOrder->fresh());
            } else {
                return response()->json([
                    'message'   => "reason cannot be empty / null"
                ], JsonResponse::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage()
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Get invoice data.
     */
    public function getInvoiceData(Request $request)
    {
        try {
            $invoice                = [];
            $invoiceItems           = [];
            $customerIds            = [];
            $uniqueCustomerIds      = [];
            if ($request->has('jo_ids') && !empty($request->jo_ids)) {

                try {
                    // this is collection
                    $jobOrders   = JobOrder::with(['customer'])->find($request->jo_ids);

                    // set init invoice data.
                    $invoice = [
                        'invoice_date'          => null,
                        'invoice_due_date'      => null,
                        'tax'                   => null,
                        'notes'                 => '',
                    ];

                    /** loop invoice items */
                    foreach ($jobOrders as $index => $jo) {
                        $invoiceItems[$index] = ([
                            'id'            => $jo->id,
                            'description'   => $jo->title,
                            'quantity'      => $jo->quantity,
                            'price'         => $jo->price,
                            'notes'         => null,
                            'discount'      => null
                        ]);
                    }
                    // merge items to the invoice array.
                    $invoice = array_merge($invoice, [
                        'items' => $invoiceItems
                    ]);

                    // is multiple job orders.
                    if (count($request->jo_ids) > 1) {

                        // set $customerIds
                        foreach ($jobOrders as $index => $jo) {
                            $customerIds = array_merge($customerIds, [$jo->customer->id]);
                        }

                        // filter unique array customerIds.
                        $uniqueCustomerIds = array_unique($customerIds);

                        if (count($uniqueCustomerIds) > 1) {
                            // different customer.
                            $allCustomers = Customer::find($uniqueCustomerIds);
                            $invoice = array_merge($invoice, ['is_multiple_customer' => true]);
                            $invoice = array_merge($invoice, ['multiple_customer_info' => new CustomerCollection($allCustomers)]);
                        } else {
                            // same customer.
                            $invoice = array_merge($invoice, ['is_multiple_customer' => false]);
                            $invoice = array_merge($invoice, [
                                'bill_to_name'          => null,
                                'bill_to_phone_number'  => null,
                                'bill_to_address'       => null,
                                'multiple_customer_info' => []
                            ]);
                        }
                    } else {
                        $invoice = array_merge($invoice, [
                            'bill_to_name'          => $jobOrders[0]->customer->name,
                            'bill_to_phone_number'  => $jobOrders[0]->customer->phone_number,
                            'bill_to_address'       => $jobOrders[0]->customer->address,
                            'is_multiple_customer'  => false,
                            'multiple_customer_info'  => [],
                        ]);
                    }
                    return response()->json($invoice);
                } catch (\Exception $exception) {
                    return response()->json([
                        'message'   => $exception->getMessage()
                    ], JsonResponse::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json([
                    'message'   => 'jo_ids cannot be null/empty.'
                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage()
            ]);
        }
    }
}
