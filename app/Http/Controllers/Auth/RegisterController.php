<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\User as UserResource;
use App\Http\Requests\RegisterUser as RegisterRequest;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  App\Http\Requests\RegisterUser $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        $credentials = $request->merge([
            'password'  => Hash::make($request->password),
        ])->only(['name', 'email', 'password']);

        $user = User::create($credentials);
        $token = $user->createToken($user->email)->plainTextToken;;
        return $this->responseWithToken($token, $user);
    }

    protected function responseWithToken($token, $user)
    {
        return response()->json([
            'success'     => true,
            'token'       => $token,
            'token_type'  => 'bearer',
            'user'        => new UserResource($user)
        ], JsonResponse::HTTP_CREATED);
    }
}
