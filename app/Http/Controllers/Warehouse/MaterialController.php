<?php

namespace App\Http\Controllers\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Requests\Warehouse\StoreMaterialRequest;
use App\Http\Resources\Warehouse\Material as MaterialResource;
use App\Http\Resources\Warehouse\MaterialCollection;
use App\Models\Material;
use App\Services\MaterialService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MaterialController extends Controller
{

    private $materialService;

    public function __construct(MaterialService $materialService)
    {
        $this->materialService = $materialService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = $this->materialService->findAll();
        return new MaterialCollection($materials);
    }

    /**
     * Search material
     */
    public function search($searchTerm)
    {
        $materials = $this->materialService->searchByKeyword($searchTerm);
        return new MaterialCollection($materials);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMaterialRequest $request)
    {
        try {
            $material = $this->materialService->create($request);
            return response()->json(
                new MaterialResource($material),
                JsonResponse::HTTP_CREATED
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $materials = $this->materialService->findById($id);
        return new MaterialResource($materials);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $material = $this->materialService->update($request, $id);
            return response()->json(
                new MaterialResource($material),
                JsonResponse::HTTP_OK
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $result = $this->materialService->delete($id);
            if ($result) return response()->json([
                'message'   => 'Material has been delete!'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }
}
