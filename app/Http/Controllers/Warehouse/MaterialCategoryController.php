<?php

namespace App\Http\Controllers\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Requests\Warehouse\StoreMaterialCategoryRequest;
use App\Http\Resources\Warehouse\MaterialCategory as MaterialCategoryResource;
use App\Http\Resources\Warehouse\MaterialCategoryCollection;
use App\Services\MaterialCategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MaterialCategoryController extends Controller
{

    private $materialCategoryService;

    public function __construct(MaterialCategoryService $materialCategoryService)
    {
        $this->materialCategoryService = $materialCategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materialCategories = $this->materialCategoryService->findAll();
        return new MaterialCategoryCollection($materialCategories);
    }

    /**
     * Search material
     */
    public function search($searchTerm)
    {
        $materialCategories = $this->materialCategoryService->searchByKeyword($searchTerm);
        return new MaterialCategoryCollection($materialCategories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMaterialCategoryRequest $request)
    {
        try {
            $materialCategory = $this->materialCategoryService->create($request);
            return response()->json(
                new MaterialCategoryResource($materialCategory),
                JsonResponse::HTTP_CREATED
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $materialCategory = $this->materialCategoryService->findById($id);
        return new MaterialCategoryResource($materialCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $materialCategory = $this->materialCategoryService->update($request, $id);
            return response()->json(
                new MaterialCategoryResource($materialCategory),
                JsonResponse::HTTP_OK
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $result = $this->materialCategoryService->delete($id);
            if ($result) return response()->json([
                'message'   => 'Material category has been delete!'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }
}
