<?php

namespace App\Http\Controllers\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Requests\Warehouse\StoreMaterialBrandRequest;
use App\Http\Resources\Warehouse\MaterialBrand as MaterialBrandResource;
use App\Http\Resources\Warehouse\MaterialBrandCollection;
use App\Services\MaterialBrandService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MaterialBrandController extends Controller
{

    private $materialBrandService;

    public function __construct(MaterialBrandService $materialBrandService)
    {
        $this->materialBrandService = $materialBrandService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materialBrands = $this->materialBrandService->findAll();
        return new MaterialBrandCollection($materialBrands);
    }

    /**
     * Search material
     */
    public function search($searchTerm)
    {
        $materialBrands = $this->materialBrandService->searchByKeyword($searchTerm);
        return new MaterialBrandCollection($materialBrands);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMaterialBrandRequest $request)
    {
        try {
            $materialBrand = $this->materialBrandService->create($request);
            return response()->json(
                new MaterialBrandResource($materialBrand),
                JsonResponse::HTTP_CREATED
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $materialBrands = $this->materialBrandService->findById($id);
        return new MaterialBrandResource($materialBrands);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $materialBrand = $this->materialBrandService->update($request, $id);
            return response()->json(
                new MaterialBrandResource($materialBrand),
                JsonResponse::HTTP_OK
            );
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $result = $this->materialBrandService->delete($id);
            if ($result) return response()->json([
                'message'   => 'Material brand has been delete!'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
            ]);
        }
    }
}
