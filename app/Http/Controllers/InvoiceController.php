<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInvoice;
use App\Http\Resources\Invoice as ResourcesInvoice;
use App\Http\Resources\InvoiceCollection;
use App\Models\Invoice;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::orderBy('id', 'desc')->paginate(25);
        return new InvoiceCollection($invoices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateInvoice $request)
    {
        $requestData = $request->only([
            'bill_to_name',
            'bill_to_address',
            'bill_to_phone_number',
            'invoice_date',
            'invoice_due_date',
            'tax',
            'bill_discount',
            'notes',
            'terms_and_conditions',
            'items'
        ]);
        try {
            $newInvoice = Invoice::create($requestData);
            if ($request->has('items') && !empty($request->items)) {
                foreach ($request->items as $inv_item) {
                    $newInvoice->items()->create([
                        'description'   => $inv_item['description'],
                        'notes'         => $inv_item['notes'],
                        'quantity'      => $inv_item['quantity'],
                        'price'         => $inv_item['price'],
                        'discount'      => $inv_item['discount'],
                    ]);
                }
            }
            return new ResourcesInvoice($newInvoice);
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::findOrFail($id);
        return new ResourcesInvoice($invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
