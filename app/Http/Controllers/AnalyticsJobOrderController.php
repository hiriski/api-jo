<?php

namespace App\Http\Controllers;

use App\Models\JobOrder;
use App\Models\JobOrderProductionStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AnalyticsJobOrderController extends Controller
{

    /**
     * Get total job order.
     */
    public function total(Request $request)
    {
        $totalJobOrder = null;
        $totalJobOrderWaiting = null;
        $totalJobOrderInProgress = null;
        $totalJobOrderDone = null;

        if (!$request->date && !$request->period) {
            $totalJobOrder             = JobOrder::get()->count();
            $totalJobOrderWaiting      = JobOrder::where('production_status_id', JobOrderProductionStatus::WAITING)->get()->count();
            $totalJobOrderInProgress   = JobOrder::where('production_status_id', JobOrderProductionStatus::IN_PROGRESS)->get()->count();
            $totalJobOrderDone         = JobOrder::where('production_status_id', JobOrderProductionStatus::DONE)->get()->count();
        } else {
            if (!$request->date) {
                if ($request->period === "week") {
                    $totalJobOrder             = JobOrder::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get()->count();
                    $totalJobOrderWaiting      = JobOrder::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('production_status_id', JobOrderProductionStatus::WAITING)->get()->count();
                    $totalJobOrderInProgress   = JobOrder::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('production_status_id', JobOrderProductionStatus::IN_PROGRESS)->get()->count();
                    $totalJobOrderDone         = JobOrder::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('production_status_id', JobOrderProductionStatus::DONE)->get()->count();
                } else if ($request->period === "month") {
                    $totalJobOrder             = JobOrder::whereMonth('created_at', date('m'))
                        ->whereYear('created_at', date('Y'))->get()->count();
                    $totalJobOrderWaiting      = JobOrder::whereMonth('created_at', date('m'))
                        ->whereYear('created_at', date('Y'))->where('production_status_id', JobOrderProductionStatus::WAITING)->get()->count();
                    $totalJobOrderInProgress   = JobOrder::whereMonth('created_at', date('m'))
                        ->whereYear('created_at', date('Y'))->where('production_status_id', JobOrderProductionStatus::IN_PROGRESS)->get()->count();
                    $totalJobOrderDone         = JobOrder::whereMonth('created_at', date('m'))
                        ->whereYear('created_at', date('Y'))->where('production_status_id', JobOrderProductionStatus::DONE)->get()->count();
                } else {
                    $totalJobOrder             = JobOrder::whereDate('created_at', Carbon::now())->get()->count();
                    $totalJobOrderWaiting      = JobOrder::whereDate('created_at', Carbon::now())->where('production_status_id', JobOrderProductionStatus::WAITING)->get()->count();
                    $totalJobOrderInProgress   = JobOrder::whereDate('created_at', Carbon::now())->where('production_status_id', JobOrderProductionStatus::IN_PROGRESS)->get()->count();
                    $totalJobOrderDone         = JobOrder::whereDate('created_at', Carbon::now())->where('production_status_id', JobOrderProductionStatus::DONE)->get()->count();
                }
            } else {
                $totalJobOrder             = JobOrder::whereDate('created_at', $request->date)->get()->count();
                $totalJobOrderWaiting      = JobOrder::whereDate('created_at', $request->date)->where('production_status_id', JobOrderProductionStatus::WAITING)->get()->count();
                $totalJobOrderInProgress   = JobOrder::whereDate('created_at', $request->date)->where('production_status_id', JobOrderProductionStatus::IN_PROGRESS)->get()->count();
                $totalJobOrderDone         = JobOrder::whereDate('created_at', $request->date)->where('production_status_id', JobOrderProductionStatus::DONE)->get()->count();
            }
        }


        return response()->json([
            'total'         => $totalJobOrder,
            'waiting'       => $totalJobOrderWaiting,
            'in_progress'   => $totalJobOrderInProgress,
            'done'          => $totalJobOrderDone,
        ]);
    }
}
