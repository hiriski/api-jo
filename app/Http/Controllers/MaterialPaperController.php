<?php

namespace App\Http\Controllers;

use App\Http\Resources\BrandMaterialPaperWithMaterialsCollection;
use App\Http\Resources\MaterialPaper as ResourcesMaterialPaper;
use App\Http\Resources\MaterialPaperCollection;
use App\Models\BrandMaterialPaper;
use App\Models\MaterialPaper;
use Illuminate\Http\Request;

class MaterialPaperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = MaterialPaper::all();
        return new MaterialPaperCollection($materials);
    }


    /**
     * index v2
     */
    public function getAllBrandsWithMaterials()
    {
        $brands = BrandMaterialPaper::with(['material_papers'])->get();
        return new BrandMaterialPaperWithMaterialsCollection($brands);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only(['name', 'brand_id', 'stock_length', 'material_width', 'length_per_roll', 'price', 'description']);
        $material = MaterialPaper::create($data);
        return new ResourcesMaterialPaper($material);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $material = MaterialPaper::findOrFail($id);
        return new ResourcesMaterialPaper($material);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only(['name', 'brand_id', 'stock_length', 'material_width', 'length_per_roll', 'price', 'description']);
        $material = MaterialPaper::findOrFail($id);
        try {
            $material->update($data);
            return new ResourcesMaterialPaper($material->fresh());
        } catch (\Exception $exception) {
            return response()->json([
                'message'   => $exception->getMessage()
            ]);
        }
    }

    /**
     * Update stock length
     */
    public function addStockLength(Request $request, $id)
    {
        $data = $request->only(['add_stock_length']);
        $material = MaterialPaper::findOrfail($id);

        $currentStockLength = $material->stock_length;
        $newStockLength = $currentStockLength + $data['add_stock_length'];

        $material->update(['stock_length' => $newStockLength]);
        return new ResourcesMaterialPaper($material);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = MaterialPaper::findOrfail($id);
        $material->delete();
    }
}
