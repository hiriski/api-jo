<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table class="table table-bordered">
      <tr>
        <td>
          {{$data->full_name}}
        </td>
        <td>
          {{$data->street_address}}
        </td>
      </tr>
      <tr>
        <td>
          {{$data->city}}
        </td>
        <td>
          {{$data->zip_code}}
        </td>
      </tr>
    </table>
  </body>
</html>