<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\JobOrderCategoryController;
use App\Http\Controllers\JobOrderController;
use App\Http\Controllers\MasterDataController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\PaymentStatusController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductionStatusController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TestEventController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TestImageController;
use App\Http\Controllers\AnalyticsJobOrderController;
use App\Http\Controllers\Warehouse\MaterialBrandController;
use App\Http\Controllers\Warehouse\MaterialCategoryController;
use App\Http\Controllers\Warehouse\MaterialController;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/test-image', TestImageController::class);
Route::post('/test-event', TestEventController::class);

Route::get('/', function () {
    return response()->json([
        'app_name'          => 'API JO',
        'message'           => 'Application is Running...',
        'description'       => '',
        'author'            => 'Riski',
    ]);
});

/** Cors middleware */
Route::middleware(['cors'])->group(function () {
    /*
    |--------------------------------------------------------
    | Authentication Routes
    |--------------------------------------------------------
    */
    Route::prefix('/auth')->group(function () {
        Route::post('/register', App\Http\Controllers\Auth\RegisterController::class)->name('register');
        Route::post('/login', App\Http\Controllers\Auth\LoginController::class)->name('login');
        Route::post('/revoke-token', [App\Http\Controllers\Auth\AuthController::class, 'revokeToken'])->name('revoke-token');
        Route::get('/get-authenticated-user', [App\Http\Controllers\Auth\AuthController::class, 'getAuthenticatedUser'])->name('get-authenticated-user');
    });


    Route::middleware(['auth:sanctum'])->group(function () {
        /*
        |--------------------------------------------------------
        | Master Data.
        |--------------------------------------------------------
        */
        Route::post('/master-data', MasterDataController::class);

        /*
        |--------------------------------------------------------
        | Product Category Routes.
        |--------------------------------------------------------
        */
        Route::apiResource('/product/category', ProductCategoryController::class);

        /*
        |--------------------------------------------------------
        | Product Routes.
        |--------------------------------------------------------
        */
        Route::get('/product/search/{searchTerm}', [ProductController::class, 'search']);
        Route::apiResource('/product', ProductController::class);

        /*
        /*
        |--------------------------------------------------------
        | Role
        |--------------------------------------------------------
        */
        Route::apiResource('/role', RoleController::class);

        /*
        /*
        |--------------------------------------------------------
        | User management.
        |--------------------------------------------------------
        */
        Route::apiResource('/user', UserController::class);

        /*
        |--------------------------------------------------------
        | Job Order Routes.
        |--------------------------------------------------------
        */
        Route::get('/job-order/search/{searchTerm}', [JobOrderController::class, 'search']);
        Route::post('/job-order/get-invoice-data', [JobOrderController::class, 'getInvoiceData']);

        Route::apiResource('/job-order/category', JobOrderCategoryController::class);

        Route::apiResource('/job-order/payment-status', PaymentStatusController::class);

        Route::apiResource('/job-order/production-status', ProductionStatusController::class);

        Route::get('/job-order/download-invoice', [JobOrderController::class, 'downloadInvoice'])
            ->name('jo.invoice');

        Route::put('/job-order/update-payment/{id}', [JobOrderController::class, 'updatePayment']);

        Route::put('/job-order/update-production-status/{id}', [JobOrderController::class, 'updateProductionStatus']);

        Route::put('/job-order/update-percentage-progress/{id}', [JobOrderController::class, 'updatePercentageProgress']);

        Route::put('/job-order/reject/{id}', [JobOrderController::class, 'reject']);

        Route::put('/job-order/add-note/{id}', [JobOrderController::class, 'addNote']);

        Route::apiResource('/job-order', JobOrderController::class);


        // Payment method
        Route::apiResource('/payment-method', PaymentMethodController::class);



        /*
        |--------------------------------------------------------
        | Customer
        |--------------------------------------------------------
        */
        Route::get('/customer/search/{searchTerm}', [CustomerController::class, 'search']);
        Route::post('/customer/address/delete', [CustomerController::class, 'deleteAddress']);
        Route::apiResource('/customer', CustomerController::class);

        /*
        |--------------------------------------------------------
        | Warehouse/Stock Opname 
        |--------------------------------------------------------
        */
        Route::prefix('/warehouse')->group(function () {

            /*
            |--------------------------------------------------------
            | Material Brand routes.
            |--------------------------------------------------------
            */
            Route::get('/material/brand/search/{searchTerm}', [MaterialBrandController::class, 'search']);
            Route::apiResource('/material/brand', MaterialBrandController::class);

            /*
            /*
            |--------------------------------------------------------
            | Material Category routes.
            |--------------------------------------------------------
            */
            Route::get('/material/category/search/{searchTerm}', [MaterialCategoryController::class, 'search']);
            Route::apiResource('/material/category', MaterialCategoryController::class);

            /*
            |--------------------------------------------------------
            | Material routes.
            |--------------------------------------------------------
            */
            Route::get('/material/search/{searchTerm}', [MaterialController::class, 'search']);
            Route::apiResource('/material', MaterialController::class);
        });

        /*
        |--------------------------------------------------------
        | Invoice routes
        |--------------------------------------------------------
        */
        Route::apiResource('/invoice', InvoiceController::class);


        /*
        |--------------------------------------------------------
        | Dashboard analytics routes.
        |--------------------------------------------------------
        */
        Route::get('/analytics/job-order/total', [AnalyticsJobOrderController::class, 'total']);
    });
});
