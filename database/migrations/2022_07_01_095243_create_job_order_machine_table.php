<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobOrderMachineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_order_machine', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('machine_id');
            $table->uuid('job_order_id');
            $table->timestamps();

            $table->foreign('machine_id')->references('id')->on('machines');
            $table->foreign('job_order_id')->references('id')->on('job_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_machine');
    }
}
