<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsTable extends Migration
{

    private $units = [
        'mm', 'cm', 'm', 'inch', 'liter', 'kg', 'pcs', 'sheet', 'ream', 'roll'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->enum('unit', $this->units);
            $table->enum('display_unit', $this->units);
            $table->string('name')->comment('Nama bahan material');
            $table->unsignedTinyInteger('material_brand_id')->nullable();
            $table->unsignedTinyInteger('material_category_id');
            $table->unsignedBigInteger('stock_length')->nullable()
                ->comment('Total panjang stock yang tersedia saat ini untuk jenis bahan kertas');
            $table->unsignedBigInteger('stock_width')->nullable()
                ->comment('Total lebar stock yang tersedia untuk jenis bahan kertas');
            $table->unsignedBigInteger('stock_pcs')->nullable()
                ->comment('Total jumlah stock yang tersedia untuk jenis yang dapat dihitung misalnya tripod untuk xbanner');
            $table->unsignedInteger('width_per_roll')->nullable()
                ->comment('Lebar material per roll');
            $table->unsignedInteger('length_per_roll')->nullable()
                ->comment('Panjang material per roll');
            $table->unsignedBigInteger('price')->nullable();
            $table->text('description')->nullable();
            $table->string('image_url')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
