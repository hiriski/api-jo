<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('invoice_number')->unique();
            $table->text('bill_to_name')->nullable();
            $table->text('bill_to_address')->nullable();
            $table->text('bill_to_phone_number')->nullable();
            $table->timestamp('invoice_date');
            $table->timestamp('invoice_due_date')->nullable();
            $table->unsignedBigInteger('tax')->nullable();
            $table->unsignedInteger('bill_discount')->nullable();
            $table->text('notes')->nullable();
            $table->text('terms_and_conditions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
