<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('created_by_user_id');
            $table->uuid('updated_by_user_id')->nullable();
            $table->uuid('product_id')->nullable();
            $table->uuid('customer_id')->nullable();
            $table->unsignedTinyInteger('status_id');
            $table->unsignedTinyInteger('category_id');
            $table->unsignedInteger('payment_status_id');
            $table->unsignedInteger('payment_method_id')->nullable();
            $table->unsignedTinyInteger('bank_account_id')->nullable();
            $table->unsignedTinyInteger('production_status_id');
            $table->boolean('is_ready_to_production')->default(false);
            $table->string('title');
            $table->string('order_number')->unique();
            $table->timestamp('order_date')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->text('body')->nullable();
            $table->unsignedBigInteger('down_payment')->nullable();
            $table->unsignedInteger('dimension_material_length');
            $table->unsignedInteger('dimension_material_width');
            $table->unsignedInteger('quantity');
            $table->unsignedBigInteger('price')->nullable();
            $table->unsignedBigInteger('tax')->nullable();
            $table->text('additional_price_name')->nullable();
            $table->unsignedBigInteger('additional_price')->nullable();
            $table->text('reason_rejected')->nullable();
            $table->uuid('rejected_by_user_id')->nullable();
            $table->boolean('is_approved')
                ->default(false)
                ->comment('Approved by ppic/leader');
            $table->unsignedTinyInteger('percentage_progress')->default(0);
            $table->uuid('progress_by_user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_orders');
    }
}
