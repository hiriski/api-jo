<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialJobOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Intermediate table between JobOrder & Material
        Schema::create('material_job_order', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('material_id');
            $table->uuid('job_order_id');
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('materials');
            $table->foreign('job_order_id')->references('id')->on('job_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_job_order');
    }
}
