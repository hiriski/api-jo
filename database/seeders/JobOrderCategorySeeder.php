<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class JobOrderCategorySeeder extends Seeder
{

    protected $tableName = 'job_order_categories';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $brands = [
            [
                'id'                => 1,
                'name'              => 'Print Stiker',
                'slug'              => Str::slug('Print Stiker'),
                'description'       => null
            ],
            [
                'id'                => 2,
                'name'              => 'Laser Cutting',
                'slug'              => Str::slug('Laser Cutting'),
                'description'       => null
            ],
            [
                'id'                => 3,
                'name'              => 'Print UV',
                'slug'              => Str::slug('Print UV'),
                'description'       => null
            ],
        ];
        DB::table($this->tableName)->insert($brands);
    }
}
