<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    protected $tableName = "users";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();

        $users = [
            [
                "id"            => Str::uuid()->toString(),
                "name"          => "Leon S. Kennedy",
                "email"         => "superadmin@yopmail.com",
                "password"      => Hash::make("password"),
                "role_id"       => 1,
                "photo_url"     => null,
                "created_at"    => Carbon::now()
            ],
            [
                "id"            => Str::uuid()->toString(),
                "name"          => "Admin",
                "email"         => "admin@yopmail.com",
                "password"      => Hash::make("password"),
                "role_id"       => 2,
                "photo_url"     => null,
                "created_at"    => Carbon::now()
            ],
            [
                "id"            => Str::uuid()->toString(),
                "name"          => "Tim Gudang",
                "email"         => "timgudang@yopmail.com",
                "password"      => Hash::make("password"),
                "role_id"       => 3,
                "photo_url"     => null,
                "created_at"    => Carbon::now()
            ],
            [
                "id"            => Str::uuid()->toString(),
                "name"          => "Tim Produksi",
                "email"         => "timproduksi@yopmail.com",
                "password"      => Hash::make("password"),
                "role_id"       => 4,
                "photo_url"     => null,
                "created_at"    => Carbon::now()
            ],
            [
                "id"            => Str::uuid()->toString(),
                "name"          => "Owner",
                "email"         => "owner@yopmail.com",
                "password"      => Hash::make("password"),
                "role_id"       => 5,
                "photo_url"     => null,
                "created_at"    => Carbon::now()
            ],
            [
                "id"            => Str::uuid()->toString(),
                "name"          => "Cashier",
                "email"         => "cashier@yopmail.com",
                "password"      => Hash::make("password"),
                "role_id"       => 7,
                "photo_url"     => null,
                "created_at"    => Carbon::now()
            ],
            [
                "id"            => Str::uuid()->toString(),
                "name"          => "CS 1",
                "email"         => "cs@yopmail.com",
                "password"      => Hash::make("password"),
                "role_id"       => 8,
                "photo_url"     => null,
                "created_at"    => Carbon::now()
            ],
        ];
        DB::table($this->tableName)->insert($users);
    }
}
