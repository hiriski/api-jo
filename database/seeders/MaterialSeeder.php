<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialSeeder extends Seeder
{

    private $tableName = 'materials';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $materials = [
            [
                'id'                    => 1,
                'name'                  => 'Rirtama ukuran 126CM / 1.26M',
                'unit'                  => 'cm',
                'display_unit'          => 'roll',
                'material_category_id'  => 1,
                'material_brand_id'     => 1,
                'stock_length'          => 5000,
                'stock_width'           => 126,
                'width_per_roll'        => 126,
                'length_per_roll'       => 5000,
                'description'           => null,
                'status'                => 'active',
                'created_at'            => Carbon::now(),
            ],
            [
                'id'                    => 2,
                'name'                  => 'Oracal ukuran 126CM / 1.26M',
                'unit'                  => 'cm',
                'display_unit'          => 'roll',
                'material_category_id'  => 1,
                'material_brand_id'     => 2,
                'stock_length'          => 5000,
                'stock_width'           => 126,
                'width_per_roll'        => 126,
                'length_per_roll'       => 5000,
                'description'           => null,
                'status'                => 'active',
                'created_at'            => Carbon::now(),
            ],
            [
                'id'                    => 3,
                'name'                  => '3M ukuran 126CM / 1.26M',
                'unit'                  => 'cm',
                'display_unit'          => 'roll',
                'material_category_id'  => 1,
                'material_brand_id'     => 2,
                'stock_length'          => 5000,
                'stock_width'           => 126,
                'width_per_roll'        => 126,
                'length_per_roll'       => 5000,
                'description'           => null,
                'status'                => 'active',
                'created_at'            => Carbon::now(),
            ],
            [
                'id'                    => 4,
                'name'                  => 'Bahan Stiker China Ukuran 126CM / 1.26M',
                'unit'                  => 'cm',
                'display_unit'          => 'roll',
                'material_category_id'  => 1,
                'material_brand_id'     => 4,
                'stock_length'          => 5000,
                'stock_width'           => 126,
                'width_per_roll'        => 126,
                'length_per_roll'       => 5000,
                'description'           => null,
                'status'                => 'active',
                'created_at'            => Carbon::now(),
            ],
            [
                'id'                    => 5,
                'name'                  => 'Bahan Spanduk Flexi China',
                'unit'                  => 'cm',
                'display_unit'          => 'roll',
                'material_category_id'  => 2,
                'material_brand_id'     => 5,
                'stock_length'          => 5000,
                'stock_width'           => 220,
                'width_per_roll'        => 220,
                'length_per_roll'       => 5000,
                'description'           => null,
                'status'                => 'active',
                'created_at'            => Carbon::now(),
            ],
            [
                'id'                    => 6,
                'name'                  => 'Bahan Spanduk Flexi China',
                'unit'                  => 'cm',
                'display_unit'          => 'roll',
                'material_category_id'  => 2,
                'material_brand_id'     => 5,
                'stock_length'          => 5000,
                'stock_width'           => 250,
                'width_per_roll'        => 250,
                'length_per_roll'       => 5000,
                'description'           => null,
                'status'                => 'active',
                'created_at'            => Carbon::now(),
            ],
        ];
        DB::table($this->tableName)->insert($materials);
    }
}
