<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // Customer::factory()->count(100)->create();
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            JobOrderProductionStatusSeeder::class,
            JobOrderCategorySeeder::class,
            PaymentStatusSeeder::class,
            BankAccountSeeder::class,
            PermissionSeeder::class,
            RolePermissionSeeder::class,
            PaymentMethodSeeder::class,
            JobOrderStatusSeeder::class,
            ProductCategorySeeder::class,
            MaterialCategorySeeder::class,
            MaterialBrandSeeder::class,
            MaterialSeeder::class,
            MachineSeeder::class,
        ]);
    }
}
