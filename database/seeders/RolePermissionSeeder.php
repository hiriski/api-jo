<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolePermissionSeeder extends Seeder
{

    protected $tableName = 'role_permission';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $rolePermissions = [
            // SA
            [
                'role_id'           => 1,
                'permission_id'     => 1
            ],
            [
                'role_id'           => 1,
                'permission_id'     => 2
            ],
            [
                'role_id'           => 1,
                'permission_id'     => 3
            ],
            [
                'role_id'           => 1,
                'permission_id'     => 4
            ],
            [
                'role_id'           => 1,
                'permission_id'     => 5
            ],
            [
                'role_id'           => 1,
                'permission_id'     => 6
            ],
            [
                'role_id'           => 1,
                'permission_id'     => 7
            ],
            [
                'role_id'           => 1,
                'permission_id'     => 8
            ],

            // Management
            [
                'role_id'           => 6,
                'permission_id'     => 1
            ],
            [
                'role_id'           => 6,
                'permission_id'     => 2
            ],
            [
                'role_id'           => 6,
                'permission_id'     => 3
            ],
            [
                'role_id'           => 6,
                'permission_id'     => 4
            ],
            [
                'role_id'           => 6,
                'permission_id'     => 5
            ],
            [
                'role_id'           => 6,
                'permission_id'     => 6
            ],
            [
                'role_id'           => 6,
                'permission_id'     => 7
            ],
            [
                'role_id'           => 6,
                'permission_id'     => 8
            ],

            // Customer Service
            [
                'role_id'           => 8,
                'permission_id'     => 1
            ],
            [
                'role_id'           => 8,
                'permission_id'     => 2
            ],
            [
                'role_id'           => 8,
                'permission_id'     => 3
            ],
            [
                'role_id'           => 8,
                'permission_id'     => 4
            ],

            // Cashier
            [
                'role_id'           => 7,
                'permission_id'     => 1
            ],
            [
                'role_id'           => 7,
                'permission_id'     => 3
            ],

            // Production Team
            [
                'role_id'           => 4,
                'permission_id'     => 1
            ],
            [
                'role_id'           => 4,
                'permission_id'     => 3
            ],

            // Warehouse Team
            [
                'role_id'           => 3,
                'permission_id'     => 5
            ],
            [
                'role_id'           => 3,
                'permission_id'     => 6
            ],
            [
                'role_id'           => 3,
                'permission_id'     => 7
            ],
            [
                'role_id'           => 3,
                'permission_id'     => 8
            ],
        ];
        DB::table($this->tableName)->insert($rolePermissions);
    }
}
