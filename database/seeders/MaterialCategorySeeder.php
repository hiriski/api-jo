<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialCategorySeeder extends Seeder
{

    private $tableName = 'material_categories';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $materialCategories = [
            [
                'id'                => 1,
                'name'              => 'Bahan Kertas Stiker (Roll)',
                'unit_material'     => 'cm',
                'description'       => null
            ],
            [
                'id'                => 2,
                'name'              => 'Bahan Spanduk/Banner/Baliho (Roll)',
                'unit_material'     => 'cm',
                'description'       => null
            ],
            [
                'id'                => 3,
                'name'              => 'Bahan Art Paper/Manila (Lembaran)',
                'unit_material'     => 'sheet',
                'description'       => null
            ],
        ];
        DB::table($this->tableName)->insert($materialCategories);
    }
}
