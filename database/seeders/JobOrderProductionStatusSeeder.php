<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class JobOrderProductionStatusSeeder extends Seeder
{

    protected $tableName = 'job_order_production_statuses';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $joStatuses = [
            [
                'id'            => 1,
                'name'          => 'Waiting',
                'slug'          => Str::slug("Waiting"),
                'description'   => "Status jo menunggu untuk dikerjakan (Waiting list)",
            ],
            [
                'id'            => 2,
                'name'          => 'In Progress',
                'slug'          => Str::slug('In Progress'),
                'description'   => "Status jo sedang dikerjakan (In Progress)",
            ],
            [
                'id'            => 3,
                'name'          => 'Selesai',
                'slug'          => Str::slug("Done"),
                'description'   => "Status jo sudah selesai dikerjakan (Done)",
            ],
            [
                'id'            => 4,
                'name'          => 'Ditahan',
                'slug'          => Str::slug("Hold"),
                'description'   => "Status jo ditahan untuk sementara karena alasan tertentu (Hold)",
            ],
            [
                'id'            => 5,
                'name'          => 'Ditolak',
                'slug'          => Str::slug("Rejected"),
                'description'   => "Status ditolak dengan alasan tertentu (Rejected)",
            ],
        ];
        DB::table($this->tableName)->insert($joStatuses);
    }
}
