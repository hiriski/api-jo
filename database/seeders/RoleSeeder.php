<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    protected $tableName = "roles";
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();

        $roles = [
            [
                'id'            => 1,
                'name'          => 'Super Admin',
                'slug'          => Str::slug('Super Admin'),
                'description'   => null
            ],
            [
                'id'            => 2,
                'name'          => 'Admin',
                'slug'          => Str::slug('Admin'),
                'description'   => null
            ],
            [
                'id'            => 3,
                'name'         => 'Tim Gudang',
                'slug'          => Str::slug('Warehouse Team'),
                'description'   => null
            ],
            [
                'id'            => 4,
                'name'         => 'Tim Produksi',
                'slug'          => Str::slug('Production Team'),
                'description'   => null
            ],
            [
                'id'            => 5,
                'name'         => 'Owner',
                'slug'          => Str::slug('Owner'),
                'description'   => null
            ],
            [
                'id'            => 6,
                'name'         => 'Management',
                'slug'          => Str::slug('Management'),
                'description'   => null
            ],
            [
                'id'            => 7,
                'name'          => 'Cashier',
                'slug'          => Str::slug('Cashier'),
                'description'   => null
            ],
            [
                'id'            => 8,
                'name'          => 'Customer Service (CS)',
                'slug'          => Str::slug('Customer Service'),
                'description'   => null
            ],
            [
                'id'            => 9,
                'name'         => 'Maintainer',
                'slug'          => Str::slug('Maintainer'),
                'description'   => null
            ],
        ];
        DB::table($this->tableName)->insert($roles);
    }
}
