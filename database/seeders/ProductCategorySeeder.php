<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductCategorySeeder extends Seeder
{

    private $tableName = 'product_categories';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $productCategories = [
            [
                'id'                => 1,
                'name'              => 'Category A',
                'slug'              => Str::slug('Category A'),
                'description'       => null
            ],
            [
                'id'                => 2,
                'name'              => 'Category B',
                'slug'              => Str::slug('Category B'),
                'description'       => null
            ],
            [
                'id'                => 3,
                'name'              => 'Category C',
                'slug'              => Str::slug('Category C'),
                'description'       => null
            ],
        ];
        DB::table($this->tableName)->insert($productCategories);
    }
}
