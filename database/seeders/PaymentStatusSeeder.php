<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PaymentStatusSeeder extends Seeder
{

    protected $tableName = "payment_statuses";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $paymentStatuses = [
            [
                'id'            => 1,
                'name'          => 'Unpaid',
                'slug'          => Str::slug("Unpaid"),
                'description'   => "Belum dibayar (Unpaid)",
            ],
            [
                'id'            => 2,
                'name'          => 'DP',
                'slug'          => Str::slug('DP'),
                'description'   => "Dibayar sebagian dari total harga (Down Payment)",
            ],
            [
                'id'            => 3,
                'name'          => 'Paid',
                'slug'          => Str::slug('Paid'),
                'description'   => "Lunas (Paid)",
            ],
        ];
        DB::table($this->tableName)->insert($paymentStatuses);
    }
}
