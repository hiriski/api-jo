<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BankAccountSeeder extends Seeder
{

    private $tableName = 'bank_accounts';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $bankAccounts = [
            [
                'id'                => 1,
                'name'              => 'Bank BCA',
                'description'       => null,
                'account_name'      => 'Test Bank',
                'account_number'    => '1111111111',
                'image_url'         => '/images/payment/bca.png',
                'status'            => 'active'
            ],
            [
                'id'                => 2,
                'name'              => 'Bank Mandiri',
                'description'       => null,
                'account_name'      => 'Test Bank Mandiri',
                'account_number'    => '1111111111',
                'image_url'         => '/images/payment/mandiri.png',
                'status'            => 'active'
            ],
        ];
        DB::table($this->tableName)->insert($bankAccounts);
    }
}
