<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{

    protected $tableName = 'permissions';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $permissions = [
            [
                'id'                => 1,
                'name'              => 'Lihat Jo',
                'code'              => 'READ_JO',
                'description'       => null
            ],
            [
                'id'                => 2,
                'name'              => 'Buat JO',
                'code'              => 'CREATE_JO',
                'description'       => null
            ],
            [
                'id'                => 3,
                'name'              => 'Edit Jo',
                'code'              => 'UPDATE_JO',
                'description'       => null
            ],
            [
                'id'                => 4,
                'name'              => 'Hapus Jo',
                'code'              => 'DELETE_JO',
                'description'       => null
            ],

            [
                'id'                => 5,
                'name'              => 'Lihat Inventory',
                'code'              => 'READ_INVENTORY',
                'description'       => null
            ],
            [
                'id'                => 6,
                'name'              => 'Buat Inventory',
                'code'              => 'CREATE_INVENTORY',
                'description'       => null
            ],
            [
                'id'                => 7,
                'name'              => 'Edit Inventory',
                'code'              => 'UPDATE_INVENTORY',
                'description'       => null
            ],
            [
                'id'                => 8,
                'name'              => 'Hapus Inventory',
                'code'              => 'DELETE_INVENTORY',
                'description'       => null
            ],
        ];
        DB::table($this->tableName)->insert($permissions);
    }
}
