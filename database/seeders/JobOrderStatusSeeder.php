<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobOrderStatusSeeder extends Seeder
{

    protected $tableName = 'job_order_statuses';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $brands = [
            [
                'id'                => 1,
                'name'              => 'Active',
                'description'       => 'Job Order aktif',
            ],
            [
                'id'                => 2,
                'name'              => 'Inactive',
                'description'       => 'Job Order tidak aktif',
            ],
            [
                'id'                => 3,
                'name'              => 'Rejected',
                'description'       => 'Job Order ditolak',
            ],
        ];
        DB::table($this->tableName)->insert($brands);
    }
}
