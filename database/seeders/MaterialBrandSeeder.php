<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialBrandSeeder extends Seeder
{

    private $tableName = 'material_brands';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $brands = [
            [
                'id'                => 1,
                'name'              => 'Ritrama',
                'description'       => null
            ],
            [
                'id'                => 2,
                'name'              => 'Oracal',
                'description'       => null
            ],
            [
                'id'                => 3,
                'name'              => '3M',
                'description'       => null
            ],
            [
                'id'                => 4,
                'name'              => 'Bahan China',
                'description'       => null
            ],
            [
                'id'                => 5,
                'name'              => 'Flexi',
                'description'       => null
            ],
        ];
        DB::table($this->tableName)->insert($brands);
    }
}
