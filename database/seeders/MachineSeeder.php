<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MachineSeeder extends Seeder
{

    private $tableName = 'machines';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $machines = [
            [
                'id'                => 1,
                'name'              => 'SpeederJet 70PT',
                'description'       => null,
                'notes'             => 'Mesin utama yang saat ini dipake untuk cetak stiker',
                'status'            => 'active'
            ],
        ];
        DB::table($this->tableName)->insert($machines);
    }
}
