<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentMethodSeeder extends Seeder
{

    protected $tableName = 'payment_methods';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->tableName)->delete();
        $paymentMethods = [
            [
                'id'            => 1,
                'name'          => 'Cash',
                'description'   => 'Metode pembayaran cash ti toko.',
                'status'        => 'active',
            ],
            [
                'id'            => 2,
                'name'          => 'Transfer Bank',
                'description'   => 'Metode pembayaran transfer bank',
                'status'        => 'active'
            ]
        ];
        DB::table($this->tableName)->insert($paymentMethods);
    }
}
